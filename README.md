Set of repositories for building ROS version of [LSD-SLAM](https://github.com/apl-ocean-engineering/lsd-slam/).

Use with [vcstool](https://github.com/dirk-thomas/vcstool) and [catkin tools](https://catkin-tools.readthedocs.io/en/latest/).

 * `lsd_slam_ros.repos` pulls core lsd-slam codebase from HTTPS
 * `lsd_slam_ros_https.repos` pulls core lsd-slam codebase via authenticated ssh
 * `lsd_slam_ros_pangolin_gui.repos` pulls core lsd-slam codebase with GUI from HTTPS
 * `lsd_slam_ros_https.repos` pulls core lsd-slam codebase with GUI  via authenticated ssh

Rough instructions:

    mkdir -p workspace/src/ && cd workspace/src/
    git clone https://gitlab.com/apl-ocean-engineering/lsd-slam/lsd-slam-ros-repos
    vcs import < lsd-slam-ros-repos/lsd_slam_ros.repos

    cd ..
    catkin config --no-install --extend /opt/ros/{distro}
    catkin config --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo

    catkin build
